---
title: "OpenPOWER ADG Super Computing 22"
eventdates:
  from: 2022-11-12
eventplace: "Dallas,Texas - USA"
#callforpresentation: 
register: https://registration.openpower.foundation/academic/ADGSC22/
#schedule:
#recordings:
#sponsors:
#  -
date: 2022-08-26
draft: false
---

Distinguished academic  experts and Industry leaders will speak/present on AI,HPC and Cloud  topics, including, current problems and technical challenges in their areas of research. This Workshop will foster open collaboration between industry and academia, led by the OpenPOWER foundation , OpenPOWER partners and IBM to address current challenges and open problems in Chip Design , AI,HPC and Cloud



{{< prereg organizer="academic" event="ADGSC22" >}}