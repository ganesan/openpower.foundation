---
title: University of Oregon Exascale Computing Center
member: universityoforegon
projects:
  - Open Source
provides:
  - Virtual Machine
  - Container
  - GPU
  - FPGA
systems:
  - POWER9
  - POWER8
operatingsystems:
  - Fedora
  - Debian
weight: -7000
date: 2022-08-24
draft: false
---
