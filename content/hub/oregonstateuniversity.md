---
title: OSU Open Source Lab
member: oregonstateuniversity
projects:
  - Open Source
provides:
  - Bare Metal
  - Virtual Machine
  - Container
  - GPU
  - FPGA
systems:
  - POWER9
  - POWER8
operatingsystems:
  - CentOS
  - Ubuntu
weight: -9000
date: 2022-08-24
draft: false
---
