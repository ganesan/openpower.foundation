---
title: IBM POWER9 Functional Simulator
link: https://www14.software.ibm.com/webapp/set2/sas/f/pwrfs/pwr9/home.html
tags:
  - power9
  - simulator
  - cpu
  - processor
  - isa
date: 2022-01-09
draft: false
---

The IBM POWER9 Functional Simulator is a simulation environment developed by IBM.
It is designed to provide enough POWER9 processor complex functionality to allow the entire software stack to execute,
including loading, booting and running a little endian Linux environment.
The intent for this tool is to educate, enable new application development,
and to facilitate porting of existing Linux applications to the POWER9 architecture.
While the IBM POWER9 Functional Simulator serves as a full instruction set simulator for the POWER9 processor,
it may not model all aspects of the IBM Power Systems POWER9 hardware and thus may not exactly reflect the behavior of the POWER9 hardware.
